<?php
  error_reporting(E_ERROR);

  include("functions/databaseConnection.php");
  session_start();
  if($_SESSION['loginState'] == false)
  {
    header('Location: index.php');
  }
  $ownerAccId = $_SESSION['id'];
 ?>

<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ticketsystem - Tickets</title>
    <meta name="autor" content="Jens Heyn">
    <link rel="stylesheet" href="style/styleMaster.css" type="text/css">
    <link rel="stylesheet" href="style/styleUserView.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="menu">
      <p>Ticketsystem</p>
      <a href="board.php" >Übersicht</a>
      <a href="ticketAnzeigen.php" id="activeIcon">Ticket anzeigen</a>
      <a href="neu.php">Neues Ticket</a>
      <a href="einstellungen.php">Einstellungen</a>
      <a href="logout.php" id="logoutIcon">Logout</a>
    </div>

    <div class="main">
      <form class="formTicketSuche" action="ticketSuche.php" method="post">
        <input required type="text" class="textbox" name="textboxSuche" placeholder="Suchbegriff hier eingeben..." value="<?php echo $_POST['textboxSuche']; ?>">
        <input type="submit" value="Suchen" id="suchenButton" class="btn btn-success" style="margin-left: 5px; padding: 10px 20px;">
      </form>

      <?php
          $suche = $_POST['textboxSuche'];
          $dbSelect = "SELECT id, subject, status FROM SupportTicket WHERE ownerAccId = $ownerAccId AND subject LIKE '%$suche%'";
          $dbResult = mysqli_query( $db, $dbSelect );

          if ( ! $dbResult )
          {
            die('Ungültige Abfrage: ' . mysqli_error());
          }
          else
          {
            echo "<br> <h3>Suchergebnisse (". mysqli_num_rows($dbResult). ")</h3>";
            echo "  <table>
                <tr>
                  <th width=100></th>
                  <th width=100></th>
                  <th width=500></th>
                </tr>";
            while ($zeile = mysqli_fetch_array( $dbResult, MYSQLI_ASSOC))
            {
              $link = "ticketVollansicht.php?id=" . $zeile['id'];

              echo "<tr style='padding: 5px; border:none;'>";
              echo "<td style='padding: 5px; border:none;'>Ticket ". $zeile['id'] . "</td>";
              if(strcmp($zeile['status'], "inProgress") == 0)
              {
                echo "<td style='padding: 5px; border:none;'>in Bearb.</td>";
              }
              elseif (strcmp($zeile['status'], "open") == 0)
              {
                echo "<td style='padding: 5px; border:none;'>Offen</td>";
              }
              elseif (strcmp($zeile['status'], "closed") == 0)
              {
                echo "<td style='padding: 5px; border:none;'>Geschlossen</td>";
              }
              echo "<td style='padding: 5px; border:none;'> <a class='link' href='" . $link . "'>" . $zeile['subject'] . "</a> </td>";
              echo "</tr>";
            }
          }
          echo "</table>";
          mysqli_free_result( $dbResult );
        ?>
    </div>
  </body>
</html>
