<?php
  error_reporting(E_ERROR);

  include("functions/databaseConnection.php");
  session_start();
  if($_SESSION['loginState'] == false)
  {
    header('Location: index.php');
  }
  $ownerAccId = $_SESSION['id'];
 ?>

<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ticketsystem - Meine Tickets</title>
    <meta name="autor" content="Jens Heyn">
    <link rel="stylesheet" href="style/styleMaster.css" type="text/css">
    <link rel="stylesheet" href="style/styleUserView.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="menu">
      <p>Ticketsystem</p>
      <a href="board.php">Übersicht</a>
      <a href="ticketAnzeigen.php" id="activeIcon">Ticket anzeigen</a>
      <a href="neu.php">Neues Ticket</a>
      <a href="einstellungen.php">Einstellungen</a>
      <a href="logout.php" id="logoutIcon">Logout</a>
    </div>

    <div class="main">
      <h3>Ticket anzeigen (ID: <?php echo $_POST['ticketId']; ?>)</h3>

      <br>
    </div>
  </body>
</html>
