<!--
  Datei: einstellungen.php
  Verwendung: Einstellungsfenster für User-Ansicht
-->

<?php
  error_reporting(E_ERROR);
  session_start();

  include("functions/databaseConnection.php");
  include("functions/einstellungenLaden.php");

  if($_SESSION['loginState'] == false)
  {
    header('Location: index.php');
  }
 ?>

<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ticketsystem - Einstellungen</title>
    <meta name="autor" content="Jens Heyn">
    <link rel="stylesheet" href="style/styleMaster.css" type="text/css">
    <link rel="stylesheet" href="style/styleUserView.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="menu">
      <p>Ticketsystem</p>
      <a href="board.php">Übersicht</a>
      <a href="ticketAnzeigen.php">Ticket anzeigen</a>
      <a href="neu.php">Neues Ticket</a>
      <a href="einstellungen.php" id="activeIcon">Einstellungen</a>
      <a href="logout.php" id="logoutIcon">Logout</a>
    </div>

    <div class="main">
      <h2>Einstellungen</h2>
      <form class="einstellungen" action="einstellungenSpeichern.php" method="post">
        <table>
          <tr>
            <th width=125></th>
            <th width=200></th>
          </tr>
          <tr>
            <td>Nummer</td>
            <td><input required type="text" name="textboxId" readonly="readonly" class="readonlyTextbox" value="<?php echo $dbDataUser['id']; ?>"></td>
          </tr>
          <tr>
            <td>Nutzername</td>
            <td><input required type="text" name="textboxUsername" readonly="readonly" class="readonlyTextbox" value="<?php echo $dbDataUser['username']; ?>"></td>
          </tr>
          <tr>
            <td>Vorname</td>
            <td><input required type="text" name="textboxVorname" class="textbox" value="<?php echo $dbDataUser['firstName']; ?>"></td>
          </tr>
          <tr>
            <td>Nachname</td>
            <td><input required type="text" name="textboxNachname" class="textbox" value="<?php echo $dbDataUser['secondName']; ?>"></td>
          </tr>
          <tr>
            <td>Abteilung</td>
            <td><input required type="text" name="textboxAbteilung" class="textbox" value="<?php echo $dbDataUser['department']; ?>"></td>
          </tr>
          <tr>
            <td>Ort</td>
            <td><input required type="text" name="textboxOrt" class="textbox" value="<?php echo $dbDataUser['location']; ?>"></td>
          </tr>
          <tr>
            <td>Raum</td>
            <td><input required type="text" name="textboxRaum" class="textbox" value="<?php echo $dbDataUser['room']; ?>"></td>
          </tr>
          <tr>
            <td>Telefon</td>
            <td><input required type="text" name="textboxTelefon" class="textbox" value="<?php echo $dbDataUser['phone']; ?>"></td>
          </tr>
          <tr>
            <td>E-Mail</td>
            <td><input required type="text" name="textboxMail" class="textbox" value="<?php echo $dbDataUser['mail']; ?>"></td>
          </tr>
          <tr>
            <td>Rechte</td>
            <td><input required type="text" name="textboxRechte" class="readonlyTextbox" readonly="readonly" value="<?php echo $rechte; ?>"></td>
          </tr>
          <tr>
            <td>Passwort</td>
            <td> <button type="button" name="button" onClick="window.location.href='passwortAendern.php'" class="btn btn-primary">Passwort ändern</button> </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input type="submit" value="Einstellungen speichern" id="speichernButton" class="btn btn-success"></td>
          </tr>
        </table>
        <br>
      </form>
    </div>
  </body>
</html>
