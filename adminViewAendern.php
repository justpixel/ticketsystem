<!--
  Datei: adminViewAendern.php
  Verwendung: Als Admin in die User-Ansicht wechseln
-->

<?php
  error_reporting(E_ERROR);
  session_start();

  if($_SESSION['adminLoginState'] == false)
  {
    header('Location: index.php');
  }
 ?>

<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ticketsystem - Admin</title>
    <meta name="autor" content="Jens Heyn">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style/styleMaster.css" type="text/css">
  </head>
  <body style="text-align: center; background-color: #404040; color: white;">
    <div class="meldung">
      <h1>Ansicht wechseln</h1>
      <p>Sie sind dabei, in die User-Ansicht zu wechseln.<br> Um erneut auf den Admin-Bereich zugreifen zu können, müssen Sie sich aus- und einloggen.</p>
      <br>
      <p>Möchten Sie wirklicht wechseln?</p>
      <br>
      <button type="button" class="btn btn-success" onClick="window.location.href='board.php'">Wechseln</button>
      <button type="button" class="btn btn-warning" onClick="window.location.href='adminUebersicht.php'">Nicht wechseln</button>
    </div>
  </body>
</html>
