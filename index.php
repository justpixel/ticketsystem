<?php
  error_reporting(E_ERROR);
  include("functions/databaseConnection.php");

  session_start();
  if($_SESSION['loginState'] == true)
  {
    header('Location: board.php');
  }
 ?>

<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="autor" content="Jens Heyn">
    <link rel="stylesheet" href="style/styleMaster.css" type="text/css">
    <link rel="stylesheet" href="style/styleIndex.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>
  <body>
    <div id="content">
      <form class="loginInput" action="index.php" method="POST">
        <h1>Willkommen zurück!</h1>
        <p>Bitte loggen Sie sich ein!</p>
        <?php
          include("functions/loginCheck.php");
         ?>
        <input required type="text" class="textbox" name="textboxUsername" placeholder="Nutzername oder E-Mail"><br>
        <input required type="password" class="textbox" name="textboxPassword" placeholder="Passwort"><br>
        <input type="submit" value="Einloggen" id="loginButton" class="btn btn-success">
        <br>
      </form>
    </div>
  </body>
</html>
