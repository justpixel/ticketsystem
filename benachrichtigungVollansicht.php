<?php
  error_reporting(E_ERROR);

  session_start();
  include("functions/databaseConnection.php");
  if($_SESSION['loginState'] == false)
  {
    header('Location: index.php');
  }

  $uri = $_SERVER['REQUEST_URI'];
  $userid = $_SESSION['id'];
  $uriTeile = explode("=", $uri);
  $benachId = $uriTeile[1];
  $dbSelectNoti = "SELECT * FROM Notification WHERE id = $benachId AND receiverAccId = $userid";
  $dbResultNoti = mysqli_query($db, $dbSelectNoti);

  if($dbResultNoti != false)
  {
    $dbCountNoti = mysqli_num_rows($dbResultNoti);
    $dbDataNoti = mysqli_fetch_array($dbResultNoti);
  }

  if(strcmp($dbDataNoti['content'], "") == 0)
  {
    echo "<script>alert('Sie haben keinen Zugriff auf diese Seite!'); window.location.href='board.php';</script>";
  }
 ?>

<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ticketsystem - Benachrichtigung</title>
    <meta name="autor" content="Jens Heyn">
    <link rel="stylesheet" href="style/styleMaster.css" type="text/css">
    <link rel="stylesheet" href="style/styleUserView.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="menu">
      <p>Ticketsystem</p>
      <a href="board.php">Übersicht</a>
      <a href="ticketAnzeigen.php">Ticket anzeigen</a>
      <a href="neu.php">Neues Ticket</a>
      <a href="einstellungen.php">Einstellungen</a>
      <a href="logout.php" id="logoutIcon">Logout</a>
    </div>

    <div class="main">
      <h3>Benachrichtigung "<?php echo $dbDataNoti['content'] ?>"</h3>


      <?php
        if($dbDataNoti['supTicId'] != 0)
        {
          $supTicId = $dbDataNoti['supTicId'];
          $dbSelectTicket = "SELECT subject FROM SupportTicket WHERE id = $supTicId";
          $dbResultTicket = mysqli_query($db, $dbSelectTicket);
          if($dbResultTicket != false)
          {
            $dbDataTicket = mysqli_fetch_array($dbResultTicket);
            echo "<p>Betroffenes Ticket: " . $dbDataTicket['subject'] . "</p>";
          }
        }
       ?>
       <br>

       <form action='gelesenBenachrichtigung.php' method='post'>
         <input type='hidden' name="notId" value="<?php echo $dbDataNoti['id'] ?>">
         <button type="button" name="button" class="btn btn-primary" onClick="window.location.href='board.php'">Zurück zur Übersicht</button>
         <input type="submit" name="gelesenButton" value="Benachrichtigung als gelesen markieren" class="btn btn-warning">
       </form>
    </div>
    <?php mysqli_close($db); ?>
  </body>
</html>
