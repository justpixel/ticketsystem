# Ticket system
A simple ticket system browser application that had been created in the course of a project work in vocational training.

## IMPORTANT
Because of security issues it's not recommended to use this tool in a life system.

## Requirements
- SQLite
- PHP (tested on version 8.0.1)
- Internet Connection (or Bootstrap locally installed)

## Installation
For installation the database/intranetTicketDB.sql must be read into a SQLite database. To connect to the database, functions/databaseConnection.php must be edited accordingly with the credentials of your database. The rest of the installation is stored in the folder of your web server.
After reading the database, an admin user must be created. For this purpose, a user is inserted into the table Account via INSERT command. The password is stored as a hash value. Don't forget to convert your password to hash first! In the field accStatus 1 is used for admin. 0 for a default user.
