<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ticketsystem - Neues Ticket</title>
    <meta name="autor" content="Jens Heyn">
    <link rel="stylesheet" href="style/styleMaster.css" type="text/css">
    <link rel="stylesheet" href="style/styleUserView.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>
  <body>
    <?php
       include("functions/databaseConnection.php");

       $username = $_POST['name'];
       $subject =  $_POST['betreff'];
       $content = $_POST['beschreibung'];

       $dbSelectUserId = "SELECT id FROM Account WHERE username = '$username'";
       $dbResultUserId = mysqli_query($db, $dbSelectUserId);

       if($dbResultUserId != false)
       {
         $dbCount = mysqli_num_rows($dbResultUserId);
         $dbData = mysqli_fetch_array($dbResultUserId);
         $ownerAccountId = $dbData['id'];

         $insert = "INSERT INTO SupportTicket (ownerAccId, subject, content, status) VALUES ($ownerAccountId ,'$subject', '$content', 'open')";
         $ergebnis = mysqli_query($db, $insert);

         $insertNot = "INSERT INTO Notification (triggerAccId, receiverAccId, supTicId, content, readNot) VALUES ($ownerAccountId, $ownerAccountId , 0, 'Sie haben ein neues Ticket ($subject) erstellt!', 'no')";
         echo $insertNot;
         $ergebnisNot = mysqli_query($db, $insertNot);

         if($ergebnis != null)
         {
           echo "
           <script type='text/javascript'>
             alert('Das Ticket wurde erfolgreich gesendet!');
             window.location.href='board.php';
           </script>
           ";
         }
         else
         {
           echo "
           <script type='text/javascript'>
             alert('Das Ticket konnte nicht gesendet werden. Fehler: Insert fehlgeschlagen');
             window.location.href='neu.php';
           </script>
           ";
         }
         mysqli_close($db);
       }
       else
       {
         echo "
         <script type='text/javascript'>
           alert('Das Ticket konnte nicht gesendet werden. Fehler: ID nicht zugeordnet');
           window.location.href='neu.php';
         </script>
         ";
       }
     ?>
  </body>
</html>
