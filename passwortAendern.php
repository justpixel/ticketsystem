<!--
  Datei: passwortAendern.php
  Verwendung: Passwort-Ändern-Fenster für Useransicht
-->

<?php
  error_reporting(E_ERROR);
  session_start();

  include("functions/databaseConnection.php");

  if($_SESSION['loginState'] == false)
  {
    header('Location: index.php');
  }

  $username = $_SESSION['username'];
 ?>

<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ticketsystem - Admin</title>
    <meta name="autor" content="Jens Heyn">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style/styleMaster.css" type="text/css">
  </head>
  <body style="text-align: center; background-color: #404040; color: white;">
    <div class="meldung">
      <h1>Passwort ändern</h1>
      <p>Sie sind dabei, Ihr persönliches Passwort zu ändern.<br> Bitte wählen Sie ein sicheres Kennwort. Merken Sie sich Ihr Kennwort.</p>
      <br>
      <p>Bitte füllen Sie die Felder aus</p>
      <br>
      <form class="neusKennwort" action="passwortAendern.php" method="POST">
        <?php
          include("functions/neuKennwortCheck.php");
         ?>
        <input required type="password" class="textbox" name="textboxAltesPasswort" placeholder="aktuelles Passwort"><br>
        <input required type="password" class="textbox" name="textboxNeuesPasswort" placeholder="neues Passwort"><br>
        <input required type="password" class="textbox" name="textboxNeuesPasswort2" placeholder="neues Passwort wiederholen"><br><br>
        <input type="submit" value="Neues Passwort festlegen" id="loginButton" class="btn btn-success">
        <button type="button" onClick="window.location.href='einstellungen.php'" name="button" class="btn btn-danger">Abbrechen</button>
        <br>
      </form>
    </div>
  </body>
</html>
