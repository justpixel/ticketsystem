<!--
  Datei: adminUebersicht.php
  Verwendung: Übersicht und Startseite für Admin-Accs 
-->


<?php
  error_reporting(E_ERROR);
  include("functions/databaseConnection.php");

  session_start();
  if($_SESSION['adminLoginState'] == false)
  {
    header('Location: index.php');
  }
 ?>

<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ticketsystem - Admin</title>
    <meta name="autor" content="Jens Heyn">
    <link rel="stylesheet" href="style/styleMaster.css" type="text/css">
    <link rel="stylesheet" href="style/styleUserView.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>
  <body>
    <<div class="menu">
      <p>Ticketsystem: Admin</p>
      <a href="adminUebersicht.php" id="activeIcon">Übersicht</a>
      <a href="adminOffeneTickets.php">Offene Tickets</a>
      <a href="adminAlleTickets.php">Alle Tickets</a>
      <a href="adminBenachrichtigungen.php">Benachrichtigungen</a>
      <a href="adminUser.php">Nutzer*innen</a>
      <a href="adminViewAendern.php">User-Ansicht</a>
      <a href="adminEinstellungen.php">Einstellungen</a>
      <a href="logout.php" id="logoutIcon">Logout</a>
    </div>

    <div class="main">
      <h2>Übersicht</h2>
      <?php
        $dbSelectAccount = "SELECT * FROM Account;";
        $dbResultAccount = mysqli_query($db, $dbSelectAccount);
        $dbCountAccount = mysqli_num_rows($dbResultAccount);

        $dbSelectAccountAdmin = "SELECT * FROM Account WHERE accStatus = 'administrator';";
        $dbResultAccountAdmin = mysqli_query($db, $dbSelectAccountAdmin);
        $dbCountAccountAdmin = mysqli_num_rows($dbResultAccountAdmin);

        $dbSelectAccountNormal = "SELECT * FROM Account WHERE accStatus = 'user';";
        $dbResultAccountNormal = mysqli_query($db, $dbSelectAccountNormal);
        $dbCountAccountNormal = mysqli_num_rows($dbResultAccountNormal);

        $dbSelectNotification = "SELECT * FROM Notification;";
        $dbResultNotification = mysqli_query($db, $dbSelectNotification);
        $dbCountNotification = mysqli_num_rows($dbResultNotification);

        $dbSelectNotificationUngelesen = "SELECT * FROM Notification WHERE readNot = false;";
        $dbResultNotificationUngelesen = mysqli_query($db, $dbSelectNotificationUngelesen);
        $dbCountNotificationUngelesen = mysqli_num_rows($dbResultNotificationUngelesen);

        $dbSelectNotificationGelesen = "SELECT * FROM Notification WHERE readNot = true;";
        $dbResultNotificationGelesen = mysqli_query($db, $dbSelectNotificationGelesen);
        $dbCountNotificationGelesen = mysqli_num_rows($dbResultNotificationGelesen);

        $dbSelectTicket = "SELECT * FROM SupportTicket;";
        $dbResultTicket = mysqli_query($db, $dbSelectTicket);
        $dbCountTicket = mysqli_num_rows($dbResultTicket);

        $dbSelectTicketInBearbeitung = "SELECT * FROM SupportTicket WHERE status = 'inProgress';";
        $dbResultTicketInBearbeitung = mysqli_query($db, $dbSelectTicketInBearbeitung);
        $dbCountTicketInBearbeitung = mysqli_num_rows($dbResultTicketInBearbeitung);

        $dbSelectTicketBeendet = "SELECT * FROM SupportTicket WHERE status = 'closed';";
        $dbResultTicketBeendet = mysqli_query($db, $dbSelectTicketBeendet);
        $dbCountTicketBeendet = mysqli_num_rows($dbResultTicketBeendet);

        $dbSelectTicketOffen = "SELECT * FROM SupportTicket WHERE status = 'open';";
        $dbResultTicketOffen = mysqli_query($db, $dbSelectTicketOffen);
        $dbCountTicketOffen = mysqli_num_rows($dbResultTicketOffen);

        echo "<p><b>Tickets ($dbCountTicket)</b><br>";
        echo "Offen: <i>$dbCountTicketOffen</i><br>";
        echo "In Bearbeitung: <i>$dbCountTicketInBearbeitung</i><br>";
        echo "Abgeschlossen: <i>$dbCountTicketBeendet</i></p>";

        echo "<p><b>Benachrichtigungen ($dbCountNotification)</b><br>";
        echo "Gelesen: <i>$dbCountNotificationGelesen</i><br>";
        echo "Ungelesen: <i>$dbCountNotificationUngelesen</i><br></p>";

        echo "<p><b>Konten ($dbCountAccount)</b><br>";
        echo "Normal: <i>$dbCountAccountNormal</i><br>";
        echo "Admin: <i>$dbCountAccountAdmin</i><br></p>";
       ?>
    </div>

  </body>
</html>
