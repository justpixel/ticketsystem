<!--
  Datei: adminBenachrichtigungen.php
  Verwendung: Zeigt alle Benachrichtigungen aller User an
-->

<?php
  error_reporting(E_ERROR);
  session_start();

  include("functions/databaseConnection.php");

  if($_SESSION['adminLoginState'] == false)
  {
    header('Location: index.php');
  }
 ?>

<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ticketsystem - Übersicht</title>
    <meta name="autor" content="Jens Heyn">
    <link rel="stylesheet" href="style/styleMaster.css" type="text/css">
    <link rel="stylesheet" href="style/styleUserView.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>
  <body>
    <<div class="menu">
      <p>Ticketsystem: Admin</p>
      <a href="adminUebersicht.php">Übersicht</a>
      <a href="adminOffeneTickets.php" id="activeIcon">Offene Tickets</a>
      <a href="adminAlleTickets.php">Alle Tickets</a>
      <a href="adminBenachrichtigungen.php">Benachrichtigungen</a>
      <a href="adminUser.php">Nutzer*innen</a>
      <a href="adminViewAendern.php">User-Ansicht</a>
      <a href="adminEinstellungen.php">Einstellungen</a>
      <a href="logout.php" id="logoutIcon">Logout</a>
    </div>

    <div class="main">
      <?php
        //
        // gelesene Benachrichtigungen
        //
        $dbSelect = "SELECT supTicId, content, id FROM Notification WHERE readNot = 'read' ORDER BY id DESC";
        $dbResult = mysqli_query( $db, $dbSelect );

        if ( ! $dbResult )
        {
          die('Fehler ' . mysqli_error());
        }
        else
        {
          echo "<br> <h3>gelesene Benachrichtigungen (". mysqli_num_rows($dbResult). ")</h3>";
          echo "  <table>
              <tr>
                <th width=100></th>
                <th width=500></th>
              </tr>";
          while ($zeile = mysqli_fetch_array( $dbResult, MYSQLI_ASSOC))
          {
            $link = "adminBenachrichtigungVollansicht.php?id=" . $zeile['id'];

            if($zeile['supTicId'] == 0)
            {
                echo "<td style='padding: 5px; border:none;'>System</td>";
            }
            else
            {
              echo "<td style='padding: 5px; border:none;'> Ticket ". $zeile['supTicId'] . "</td>";
            }
            echo "<td style='padding: 5px; border:none;'> <a class='link' href='" . $link . "'>"  . $zeile['content'] . "</a> </td>";
            echo "</tr>";
          }
        }
        echo "</table>";
        mysqli_free_result( $dbResult );
      ?>

      <?php
        //
        // ungelesene Benachrichtigungen
        //
        $dbSelect = "SELECT supTicId, content, id FROM Notification WHERE readNot = 'no' ORDER BY id DESC";
        $dbResult = mysqli_query( $db, $dbSelect );

        if ( ! $dbResult )
        {
          die('Fehler ' . mysqli_error());
        }
        else
        {
          echo "<br> <h3>ungelesene Benachrichtigungen (". mysqli_num_rows($dbResult). ")</h3>";
          echo "  <table>
              <tr>
                <th width=100></th>
                <th width=500></th>
              </tr>";
          while ($zeile = mysqli_fetch_array( $dbResult, MYSQLI_ASSOC))
          {
            $link = "adminBenachrichtigungVollansicht.php?id=" . $zeile['id'];

            if($zeile['supTicId'] == 0)
            {
                echo "<td style='padding: 5px; border:none;'>System</td>";
            }
            else
            {
              echo "<td style='padding: 5px; border:none;'> Ticket ". $zeile['supTicId'] . "</td>";
            }
            echo "<td style='padding: 5px; border:none;'> <a class='link' href='" . $link . "'>"  . $zeile['content'] . "</a> </td>";
            echo "</tr>";
          }
        }
        echo "</table>";
        mysqli_free_result( $dbResult );
      ?>
    </div>
  </body>
</html>
