<?php
  error_reporting(E_ERROR);

  session_start();
  include("functions/databaseConnection.php");
  if($_SESSION['loginState'] == false)
  {
    header('Location: index.php');
  }

  $uri = $_SERVER['REQUEST_URI'];
  $userid = $_SESSION['id'];
  $uriTeile = explode("=", $uri);
  $ticketId = $uriTeile[1];
  $dbSelectTicket = "SELECT * FROM SupportTicket WHERE id = $ticketId AND ownerAccId = $userid";
  $dbResultTicket = mysqli_query($db, $dbSelectTicket);

  if($dbResultTicket != false)
  {
    $dbCountTicket = mysqli_num_rows($dbResultTicket);
    $dbDataTicket = mysqli_fetch_array($dbResultTicket);
  }

  if(strcmp($dbDataTicket['content'], "") == 0)
  {
    echo "<script>alert('Sie haben keinen Zugriff auf diese Seite!'); window.location.href='board.php';</script>";
  }
 ?>

<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ticketsystem - Ticket</title>
    <meta name="autor" content="Jens Heyn">
    <link rel="stylesheet" href="style/styleMaster.css" type="text/css">
    <link rel="stylesheet" href="style/styleUserView.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="menu">
      <p>Ticketsystem</p>
      <a href="board.php">Übersicht</a>
      <a href="ticketAnzeigen.php" id="activeIcon">Ticket anzeigen</a>
      <a href="neu.php">Neues Ticket</a>
      <a href="einstellungen.php">Einstellungen</a>
      <a href="logout.php" id="logoutIcon">Logout</a>
    </div>

    <div class="main">
      <h3>Ticket "<?php echo $dbDataTicket['subject'] ?>"</h3>
      <?php
        if($dbDataTicket['consultantAccId'] != NULL)
        {
          $consId = $dbDataTicket['consultantAccId'];
          $dbSelectMit = "SELECT firstName, secondName FROM Account WHERE id = $consId";
          $dbResultMit = mysqli_query($db, $dbSelectMit);
          if($dbResultMit != false)
          {
            $dbDataMit = mysqli_fetch_array($dbResultMit);
            echo "<p>Supporter*in: " . $dbDataMit['secondName'] . " " . $dbDataMit['firstName'] . "</p>";
          }
          else
          {
            echo "Fehler bei MySQL-Select";
          }
        }
        else {
          echo "<p>Ihr Ticket wurde im Support-Team noch nicht zugeteilt.</p>";
        }
        if(strcmp($dbDataTicket['status'], "inProgress") == 0)
        {
          echo "Status: in Bearbeitung";
        }
        elseif (strcmp($dbDataTicket['status'], "open") == 0)
        {
          echo "Status: Offen";
        }
        elseif (strcmp($dbDataTicket['status'], "closed") == 0)
        {
          echo "Status: Geschlossen";
        }
        echo "<br><br>Problembeschreibung:<br> <textarea class='borderReadonlyTextbox' rows='8' readonly='readonly' style='width: 800px'> " . $dbDataTicket['content'] . "</textarea>";
       ?>
       <br>
       <form action="<?php echo (strcmp($dbDataTicket['status'], 'closed') == 0) ? 'oeffnenTicket.php' : 'schliessenTicket.php' ?>" method='post'>
         <input type='hidden' name="ticketId" value="<?php echo $dbDataTicket['id'] ?>">
         <button type="button" name="button" class="btn btn-primary" onClick="window.location.href='board.php'">Zurück zur Übersicht</button>
         <input type="submit" name="aendernButton" value="<?php echo (strcmp($dbDataTicket['status'], 'closed') == 0) ? 'Ticket wieder öffnen' : 'Ticket schließen' ?>" class="<?php echo (strcmp($dbDataTicket['status'], 'closed') == 0) ? 'btn btn-warning' : 'btn btn-danger' ?>">
       </form>

    </div>

    <?php mysqli_close($db); ?>
  </body>
</html>
