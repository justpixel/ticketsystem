<?php
  session_start();
  session_destroy();
?>

<script type="text/javascript">
  alert("Sie wurden ausgeloggt und werden nun automatisch zurück zum Login weitergeleitet.");
  window.location.href='index.php';
</script>
